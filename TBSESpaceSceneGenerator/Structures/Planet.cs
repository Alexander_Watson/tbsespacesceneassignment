﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator.Structures
{
    public enum PlanetType
    {
        Terrestrial,
        GiantGas,
        GiantIce,
        Count
    }

    class Planet
    {
        public PlanetType Type { get; set; }

        public bool IsHabitable { get; set; }

        public double Diameter { get; set; }

        public double Mass { get; set; }

        public double SemiMajorAxis { get; set; }

        public double OrbitalPeriod { get; set; }

        public double OrbitalEccentricity { get; set; }

        public double RotationPeriod { get; set; }

        public ConcurrentStack<Moon> Moons { get; private set; }

        public ConcurrentStack<Satellite> Satellites { get; private set; }

        public Atmosphere Atmosphere { get; set; }

        public PlanetaryRing Ring { get; set; }

        public Terrain Terrain { get; set; }

        public Planet()
        {
            Moons = new ConcurrentStack<Moon>();
            Satellites = new ConcurrentStack<Satellite>();
        }
    }
}
