﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator.Structures
{
    [Serializable]
    class SolarSystem
    {
        private static int Count = 0;

        public int ID { get; private set; }
        public Star Star { get; set; }
        public AsteroidBelt AsteroidBelt { get; set; }
        public ConcurrentStack<Planet> Planets { get; private set; }
        public ConcurrentStack<Comet> Comets { get; private set; }

        public SolarSystem()
        {
            this.ID = Count;

            Star = null;
            AsteroidBelt = null;
            Planets = new ConcurrentStack<Planet>();
            Comets = new ConcurrentStack<Comet>();

            Count++;
        }

        public static void ResetCount()
        {
            Count = 0;
        }

        public void AddPlanet(Planet planet)
        {
            Planets.Push(planet);
        }

        public void AddPlanets(ConcurrentStack<Planet> planets, int startIndex, int planetCount)
        {
            Planets.PushRange(planets.ToArray(), startIndex, planetCount);
        }

        public void AddComet(Comet comet)
        {
            Comets.Push(comet);
        }

        public void AddComets(ConcurrentStack<Comet> comets, int startIndex, int cometCount)
        {
            Comets.PushRange(comets.ToArray(), startIndex, cometCount);
        }
    }
}
