﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class PlanetGenerator
    {
        public PlanetGenerator() { }

        public Planet Generate()
        {
            int moonCount, satelliteCount;
            MoonGenerator moonGenerator;
            SatelliteGenerator satelliteGenerator;
            RandomSingleton random = RandomSingleton.Instance();

            Planet planet = new Planet();
            planet.Type = GetPlanetType();

            switch (planet.Type)
            {
                case PlanetType.Terrestrial:
                    Task.Factory.StartNew(() =>
                        {
                            planet.IsHabitable = random.NextDouble() < 0.1;
                            planet.Diameter = random.NextDouble(1.5);
                            planet.Mass = random.NextDouble() * planet.Diameter;
                            planet.SemiMajorAxis = random.NextDouble(2.0);
                            planet.OrbitalPeriod = Math.Pow(random.NextDouble() * planet.SemiMajorAxis, 1.5);
                            planet.OrbitalEccentricity = random.NextDouble(0.2);
                            planet.RotationPeriod = (random.NextDouble(2.0) / planet.SemiMajorAxis) * random.Next(2) == 0 ? 1 : -1;
                        });

                    Task.Factory.StartNew(() =>
                        {
                            moonCount = random.Next(0, 5);
                            moonGenerator = new MoonGenerator();
                            Parallel.For(0, moonCount, (i) =>
                            {
                                planet.Moons.Push(moonGenerator.Generate());
                            });
                        });

                    Task.Factory.StartNew(() =>
                        {
                            satelliteCount = random.Next(0, 250);
                            satelliteGenerator = new SatelliteGenerator();
                            Parallel.For(0, satelliteCount, (i) =>
                            {
                                planet.Satellites.Push(satelliteGenerator.Generate());
                            });
                        });

                    Task.Factory.StartNew(() =>
                        {
                            planet.Atmosphere = new AtmosphereGenerator().Generate(planet.Type, planet.SemiMajorAxis);
                            planet.Ring = new PlanetaryRingGenerator().Generate(planet.Type);
                            planet.Terrain = new TerrainGenerator().Generate(planet.Type);
                        });
                    break;
                case PlanetType.GiantGas:
                    Task.Factory.StartNew(() =>
                        {
                            planet.IsHabitable = false;
                            planet.Diameter = random.NextDouble(8.0, 15.0);
                            planet.Mass = random.NextDouble(20.0) * planet.Diameter;
                            planet.SemiMajorAxis = random.NextDouble(5.0, 10.0);
                            planet.OrbitalPeriod = random.NextDouble(3.0, 5.0) * planet.SemiMajorAxis;
                            planet.OrbitalEccentricity = random.NextDouble(0.1);
                            planet.RotationPeriod = ((random.NextDouble(5.0)) / planet.SemiMajorAxis) * random.Next(4) == 0 ? -1 : 1;
                        });

                    Task.Factory.StartNew(() =>
                        {
                            moonCount = random.Next(50, 75);
                            moonGenerator = new MoonGenerator();
                            Parallel.For(0, moonCount, (i) =>
                            {
                                planet.Moons.Push(moonGenerator.Generate());
                            });
                        });

                    Task.Factory.StartNew(() =>
                        {
                            satelliteCount = random.Next(0, 20);
                            satelliteGenerator = new SatelliteGenerator();
                            Parallel.For(0, satelliteCount, (i) =>
                            {
                                planet.Satellites.Push(satelliteGenerator.Generate());
                            });
                        });

                    Task.Factory.StartNew(() =>
                        {
                            planet.Atmosphere = new AtmosphereGenerator().Generate(planet.Type, planet.SemiMajorAxis);
                            planet.Ring = new PlanetaryRingGenerator().Generate(planet.Type);
                            planet.Terrain = new TerrainGenerator().Generate(planet.Type);
                        });
                    break;
                case PlanetType.GiantIce:
                    Task.Factory.StartNew(() =>
                        {
                            planet.IsHabitable = false;
                            planet.Diameter = random.NextDouble(3.0, 5.0);
                            planet.Mass = random.NextDouble(3.0, 4.0) * planet.Diameter;
                            planet.SemiMajorAxis = random.NextDouble(12.0, 25.0);
                            planet.OrbitalPeriod = random.NextDouble(5.0, 8.0) * planet.SemiMajorAxis;
                            planet.OrbitalEccentricity = random.NextDouble(0.1);
                            planet.RotationPeriod = ((random.NextDouble(12.0)) / planet.SemiMajorAxis) * random.Next(4) == 0 ? -1 : 1;
                        });

                    Task.Factory.StartNew(() =>
                        {
                            moonCount = random.Next(15, 35);
                            moonGenerator = new MoonGenerator();
                            Parallel.For(0, moonCount, (i) =>
                            {
                                planet.Moons.Push(moonGenerator.Generate());
                            });
                        });

                    Task.Factory.StartNew(() =>
                        {
                            satelliteCount = random.Next(0, 10);
                            satelliteGenerator = new SatelliteGenerator();
                            Parallel.For(0, satelliteCount, (i) =>
                            {
                                planet.Satellites.Push(satelliteGenerator.Generate());
                            });
                        });

                    Task.Factory.StartNew(() =>
                        {
                            planet.Atmosphere = new AtmosphereGenerator().Generate(planet.Type, planet.SemiMajorAxis);
                            planet.Ring = new PlanetaryRingGenerator().Generate(planet.Type);
                            planet.Terrain = new TerrainGenerator().Generate(planet.Type);
                        });
                    break;
            }

            return planet;
        }

        private PlanetType GetPlanetType()
        {
            RandomSingleton random = RandomSingleton.Instance();
            double typePercentage = random.NextDouble();

            if (typePercentage < 0.5)
                return PlanetType.Terrestrial;
            else if (typePercentage < 0.75)
                return PlanetType.GiantGas;
            else
                return PlanetType.GiantIce;
        }
    }
}
