﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class SolarSystemGenerator
    {
        public SolarSystemGenerator() { }

        public SolarSystem Generate()
        {
            RandomSingleton random = RandomSingleton.Instance();
            SolarSystem solarSystem = new SolarSystem();

            Task starTask = Task.Factory.StartNew(() =>
            {
                solarSystem.Star = new StarGenerator().Generate();
            });

            Task planetTask = starTask.ContinueWith(a =>
            {
                PlanetGenerator planetGenerator = new PlanetGenerator();                 
                int planetCount = random.Next((int)Math.Sqrt((double)solarSystem.Star.Class), (int)Math.Sqrt(Math.Pow((double)solarSystem.Star.Class, 3.5)));
                Parallel.For(0, planetCount, i =>
                {
                    solarSystem.Planets.Push(planetGenerator.Generate());
                });
            });

            Task cometTask = Task.Factory.StartNew(() =>
            {
                CometGenerator cometGenerator = new CometGenerator();
                int cometCount = random.Next(250);
                Parallel.For(0, cometCount, (i) =>
                {
                    solarSystem.Comets.Push(cometGenerator.Generate());
                });
            });

            Task asteroidTask = Task.Factory.StartNew(() =>
            {
                double asteroidBeltChance = random.NextDouble();
                if (asteroidBeltChance < 0.2)
                    solarSystem.AsteroidBelt = new AsteroidBeltGenerator().Generate();
            });

            Task.WaitAll(starTask, planetTask, cometTask, asteroidTask);

            return solarSystem;
        }
    }
}
