﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class PlanetaryRingGenerator
    {

        public PlanetaryRingGenerator() { }

        public PlanetaryRing Generate(PlanetType planetType)
        {
            RandomSingleton random = RandomSingleton.Instance();
            PlanetaryRing ring = new PlanetaryRing();

            switch (planetType)
            {
                case PlanetType.Terrestrial:
                    if (random.NextDouble() < 0.05)
                        ring.Composition = RingComposition.Rock;
                    else
                        return null;
                    break;
                case PlanetType.GiantGas:
                    if (random.NextDouble() < 0.95)
                        ring.Composition = RingComposition.Rock;
                    else
                        return null;
                    break;
                case PlanetType.GiantIce:
                    if (random.NextDouble() < 0.95)
                        ring.Composition = RingComposition.Ice;
                    else
                        return null;
                    break;
            }

            return ring;
        }
    }
}
