﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class UniverseGenerator
    {
        public ConcurrentQueue<SolarSystem> Generate(int seed, int solarSystemCount)
        {
            ConcurrentQueue<SolarSystem> solarSystems = new ConcurrentQueue<SolarSystem>();
            BlockingCollection<SolarSystemGenerator> solarSystemsCollection = new BlockingCollection<SolarSystemGenerator>();
            SolarSystem.ResetCount();

            RandomSingleton.Instance().SetSeed(seed);

            Task producerTask = Task.Factory.StartNew(() =>
            {
                Parallel.For(0, solarSystemCount, i =>
                {
                    solarSystemsCollection.Add(new SolarSystemGenerator());
                });
            });

            Task continueTask = producerTask.ContinueWith(a =>
            {
                solarSystemsCollection.CompleteAdding();
            });

            Task consumerTask = Task.Factory.StartNew(() =>
            {
                while(!solarSystemsCollection.IsCompleted)
                {
                    SolarSystemGenerator system;
                    if(solarSystemsCollection.TryTake(out system))
                    {
                        solarSystems.Enqueue(system.Generate());
                    }
                }
            });

            consumerTask.Wait();

            return solarSystems;
        }
    }
}
